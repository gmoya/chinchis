<?php

/**
 * Peso filter form base class.
 *
 * @package    chinchis
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePesoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'peso'           => new sfWidgetFormFilterInput(),
      'observaciones'  => new sfWidgetFormFilterInput(),
      'seguimiento_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Seguimiento'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'peso'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'observaciones'  => new sfValidatorPass(array('required' => false)),
      'seguimiento_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Seguimiento'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('peso_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Peso';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'peso'           => 'Number',
      'observaciones'  => 'Text',
      'seguimiento_id' => 'ForeignKey',
    );
  }
}
