<?php

/**
 * Chinchilla filter form base class.
 *
 * @package    chinchis
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseChinchillaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'codigo'            => new sfWidgetFormFilterInput(),
      'reproductor'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'fechanac'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'color_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Color'), 'add_empty' => true)),
      'sexo_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sexo'), 'add_empty' => true)),
      'criadero_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Criadero'), 'add_empty' => true)),
      'sectorcriadero_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SectorCriadero'), 'add_empty' => true)),
      'jaula_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Jaula'), 'add_empty' => true)),
      'fila'              => new sfWidgetFormFilterInput(),
      'usuario_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'codigo'            => new sfValidatorPass(array('required' => false)),
      'reproductor'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'fechanac'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'color_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Color'), 'column' => 'id')),
      'sexo_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Sexo'), 'column' => 'id')),
      'criadero_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Criadero'), 'column' => 'id')),
      'sectorcriadero_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SectorCriadero'), 'column' => 'id')),
      'jaula_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Jaula'), 'column' => 'id')),
      'fila'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'usuario_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('chinchilla_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Chinchilla';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'codigo'            => 'Text',
      'reproductor'       => 'Boolean',
      'fechanac'          => 'Date',
      'color_id'          => 'ForeignKey',
      'sexo_id'           => 'ForeignKey',
      'criadero_id'       => 'ForeignKey',
      'sectorcriadero_id' => 'ForeignKey',
      'jaula_id'          => 'ForeignKey',
      'fila'              => 'Number',
      'usuario_id'        => 'ForeignKey',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
    );
  }
}
