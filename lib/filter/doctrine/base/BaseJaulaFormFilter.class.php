<?php

/**
 * Jaula filter form base class.
 *
 * @package    chinchis
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseJaulaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'codigo'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'columna'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'fila'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sectorcriadero_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SectorCriadero'), 'add_empty' => true)),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'codigo'            => new sfValidatorPass(array('required' => false)),
      'columna'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fila'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'sectorcriadero_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SectorCriadero'), 'column' => 'id')),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('jaula_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Jaula';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'codigo'            => 'Text',
      'columna'           => 'Number',
      'fila'              => 'Number',
      'sectorcriadero_id' => 'ForeignKey',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
    );
  }
}
