<?php

/**
 * Estado filter form base class.
 *
 * @package    chinchis
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEstadoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'tipoestado_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoEstado'), 'add_empty' => true)),
      'seguimiento_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Seguimiento'), 'add_empty' => true)),
      'observaciones'  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'tipoestado_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TipoEstado'), 'column' => 'id')),
      'seguimiento_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Seguimiento'), 'column' => 'id')),
      'observaciones'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('estado_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Estado';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'tipoestado_id'  => 'ForeignKey',
      'seguimiento_id' => 'ForeignKey',
      'observaciones'  => 'Text',
    );
  }
}
