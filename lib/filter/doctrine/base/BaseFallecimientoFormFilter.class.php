<?php

/**
 * Fallecimiento filter form base class.
 *
 * @package    chinchis
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFallecimientoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'chinchilla_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'), 'add_empty' => true)),
      'fallecimiento_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'causa_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CausaFallecimiento'), 'add_empty' => true)),
      'otros'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'chinchilla_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Chinchilla'), 'column' => 'id')),
      'fallecimiento_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'causa_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CausaFallecimiento'), 'column' => 'id')),
      'otros'            => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fallecimiento_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Fallecimiento';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'chinchilla_id'    => 'ForeignKey',
      'fallecimiento_at' => 'Date',
      'causa_id'         => 'ForeignKey',
      'otros'            => 'Text',
    );
  }
}
