<?php

/**
 * Conducta filter form base class.
 *
 * @package    chinchis
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseConductaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'actmaternal'    => new sfWidgetFormFilterInput(),
      'otros'          => new sfWidgetFormFilterInput(),
      'seguimiento_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Seguimiento'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'actmaternal'    => new sfValidatorPass(array('required' => false)),
      'otros'          => new sfValidatorPass(array('required' => false)),
      'seguimiento_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Seguimiento'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('conducta_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Conducta';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'actmaternal'    => 'Text',
      'otros'          => 'Text',
      'seguimiento_id' => 'ForeignKey',
    );
  }
}
