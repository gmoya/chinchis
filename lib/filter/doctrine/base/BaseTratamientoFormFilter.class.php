<?php

/**
 * Tratamiento filter form base class.
 *
 * @package    chinchis
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTratamientoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'diagnostico'    => new sfWidgetFormFilterInput(),
      'descripcion'    => new sfWidgetFormFilterInput(),
      'evolucion'      => new sfWidgetFormFilterInput(),
      'seguimiento_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Seguimiento'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'diagnostico'    => new sfValidatorPass(array('required' => false)),
      'descripcion'    => new sfValidatorPass(array('required' => false)),
      'evolucion'      => new sfValidatorPass(array('required' => false)),
      'seguimiento_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Seguimiento'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('tratamiento_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Tratamiento';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'diagnostico'    => 'Text',
      'descripcion'    => 'Text',
      'evolucion'      => 'Text',
      'seguimiento_id' => 'ForeignKey',
    );
  }
}
