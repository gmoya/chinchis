<?php

class SectorCriaderoTable extends Doctrine_Table
{
	static public function getSectoresFaltantes($criadero_id)
	{
  		$q = Doctrine_Query::create()
				->from('SectorCriadero sc')
    		->where('sc.criadero_id = ?', $criadero_id);

			$sectores_c = $q->execute();
			$sectores_id = array();

			foreach ($sectores_c as $sector_c) {
					$sectores_id[] = $sector_c->getSectorId();
			}
			
			return Doctrine_Core::getTable('Sector')->filtrarFaltantes($sectores_id);
	}

}
