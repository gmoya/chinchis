<?php

class JaulaTable extends Doctrine_Table
{
	public function existe($sectorc_id, $fila, $columna)
	{
			$q = $this->createQuery('j')
				->where('j.sectorcriadero_id = ?', $sectorc_id)
				->andWhere('j.fila = ?', $fila)
				->andWhere('j.columna = ?', $columna);

			return $q->execute()->count();
	}
}
