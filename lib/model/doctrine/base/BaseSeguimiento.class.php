<?php

/**
 * BaseSeguimiento
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $tiposeguimiento_id
 * @property integer $chinchilla_id
 * @property string $observaciones
 * @property TipoSeguimiento $TipoSeguimiento
 * @property Chinchilla $Chinchilla
 * @property Doctrine_Collection $Conducta
 * @property Doctrine_Collection $Estado
 * @property Doctrine_Collection $Peso
 * @property Doctrine_Collection $Tratamiento
 * 
 * @method integer             getTiposeguimientoId()  Returns the current record's "tiposeguimiento_id" value
 * @method integer             getChinchillaId()       Returns the current record's "chinchilla_id" value
 * @method string              getObservaciones()      Returns the current record's "observaciones" value
 * @method TipoSeguimiento     getTipoSeguimiento()    Returns the current record's "TipoSeguimiento" value
 * @method Chinchilla          getChinchilla()         Returns the current record's "Chinchilla" value
 * @method Doctrine_Collection getConducta()           Returns the current record's "Conducta" collection
 * @method Doctrine_Collection getEstado()             Returns the current record's "Estado" collection
 * @method Doctrine_Collection getPeso()               Returns the current record's "Peso" collection
 * @method Doctrine_Collection getTratamiento()        Returns the current record's "Tratamiento" collection
 * @method Seguimiento         setTiposeguimientoId()  Sets the current record's "tiposeguimiento_id" value
 * @method Seguimiento         setChinchillaId()       Sets the current record's "chinchilla_id" value
 * @method Seguimiento         setObservaciones()      Sets the current record's "observaciones" value
 * @method Seguimiento         setTipoSeguimiento()    Sets the current record's "TipoSeguimiento" value
 * @method Seguimiento         setChinchilla()         Sets the current record's "Chinchilla" value
 * @method Seguimiento         setConducta()           Sets the current record's "Conducta" collection
 * @method Seguimiento         setEstado()             Sets the current record's "Estado" collection
 * @method Seguimiento         setPeso()               Sets the current record's "Peso" collection
 * @method Seguimiento         setTratamiento()        Sets the current record's "Tratamiento" collection
 * 
 * @package    chinchis
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 6820 2009-11-30 17:27:49Z jwage $
 */
abstract class BaseSeguimiento extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('seguimiento');
        $this->hasColumn('tiposeguimiento_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('chinchilla_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('observaciones', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('TipoSeguimiento', array(
             'local' => 'tiposeguimiento_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('Chinchilla', array(
             'local' => 'chinchilla_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasMany('Conducta', array(
             'local' => 'id',
             'foreign' => 'seguimiento_id'));

        $this->hasMany('Estado', array(
             'local' => 'id',
             'foreign' => 'seguimiento_id'));

        $this->hasMany('Peso', array(
             'local' => 'id',
             'foreign' => 'seguimiento_id'));

        $this->hasMany('Tratamiento', array(
             'local' => 'id',
             'foreign' => 'seguimiento_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}