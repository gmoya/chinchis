<?php

/**
 * BaseCriadero
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $nombre
 * @property integer $usuario_id
 * @property sfGuardUser $sfGuardUser
 * @property Doctrine_Collection $Chinchilla
 * @property Doctrine_Collection $SectorCriadero
 * 
 * @method string              getNombre()         Returns the current record's "nombre" value
 * @method integer             getUsuarioId()      Returns the current record's "usuario_id" value
 * @method sfGuardUser         getSfGuardUser()    Returns the current record's "sfGuardUser" value
 * @method Doctrine_Collection getChinchilla()     Returns the current record's "Chinchilla" collection
 * @method Doctrine_Collection getSectorCriadero() Returns the current record's "SectorCriadero" collection
 * @method Criadero            setNombre()         Sets the current record's "nombre" value
 * @method Criadero            setUsuarioId()      Sets the current record's "usuario_id" value
 * @method Criadero            setSfGuardUser()    Sets the current record's "sfGuardUser" value
 * @method Criadero            setChinchilla()     Sets the current record's "Chinchilla" collection
 * @method Criadero            setSectorCriadero() Sets the current record's "SectorCriadero" collection
 * 
 * @package    chinchis
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 6820 2009-11-30 17:27:49Z jwage $
 */
abstract class BaseCriadero extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('criadero');
        $this->hasColumn('nombre', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => '255',
             ));
        $this->hasColumn('usuario_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('sfGuardUser', array(
             'local' => 'usuario_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasMany('Chinchilla', array(
             'local' => 'id',
             'foreign' => 'criadero_id'));

        $this->hasMany('SectorCriadero', array(
             'local' => 'id',
             'foreign' => 'criadero_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}