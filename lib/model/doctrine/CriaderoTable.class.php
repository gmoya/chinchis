<?php

class CriaderoTable extends Doctrine_Table
{
	public function verificarPropietario($criadero_id, $usuario_id)
	{
			$q = $this->createQuery('c')
				->where('c.id = '. $criadero_id)
				->andWhere('c.usuario_id = '.$usuario_id);
			
			return ($q->execute()->count()) ? true : false;
	}
	
	public function retrieveBackendCriaderoList(Doctrine_Query $q)
	{
		$userid = sfContext::getInstance()->getUser()->getId();
		$rootAlias = $q->getRootAlias();

		$q->leftJoin($rootAlias.'.SectorCriadero s')
			->leftJoin('s.TipoSector t')
			->where($rootAlias.'.usuario_id = ?', $userid)
			->addOrderBy('s.sector_id');

		return $q;
	}
}
