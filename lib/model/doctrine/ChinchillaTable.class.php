<?php

class ChinchillaTable extends Doctrine_Table
{
	static public function getChinchisDisponibles($userid)
	{
		$q = Doctrine_Query::create()
			->from('Chinchilla c')
			->leftJoin('c.Sexo s')
			->where('c.jaula_id IS NULL')
			->andWhere('s.corto = ?', 'H')
			->andWhere('c.usuario_id = ?', $userid);
		
		$results = $q->execute();
		$chinchis = array();
		
		foreach ($results as $chinchi)
		{
			$chinchis[$chinchi->getId()] = $chinchi->getCodigo();
		}

		return $chinchis;
	}

	static public function getMachoBySectorFila($sector_id, $fila)
	{
		$q = Doctrine_Query::create()
			->from('Chinchilla c')
			->where('c.sectorcriadero_id = ?', $sector_id)
			->andWhere('c.fila = ?', $fila)
			->limit(1);
	
		return $q->execute();
	}
	
}
