<?php

class SectorTable extends Doctrine_Table
{
	static public function filtrarFaltantes($sectores_id)
	{
			$q = Doctrine_Query::create()
				->from('Sector s')
				->whereNotIn('id', $sectores_id);
			
			$sectores = $q->execute();
			$choices = array();

			foreach ($sectores as $sector)
			{
					$choices[$sector->getId()] = $sector->getId();
			}

			return $choices;
	}
}
