<?php

/**
 * SectorCriadero form base class.
 *
 * @method SectorCriadero getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSectorCriaderoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'alias'         => new sfWidgetFormInputText(),
      'criadero_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Criadero'), 'add_empty' => false)),
      'filas'         => new sfWidgetFormInputText(),
      'columnas'      => new sfWidgetFormInputText(),
      'sector_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sector'), 'add_empty' => false)),
      'tiposector_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoSector'), 'add_empty' => false)),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'alias'         => new sfValidatorString(array('max_length' => 255)),
      'criadero_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Criadero'))),
      'filas'         => new sfValidatorInteger(),
      'columnas'      => new sfValidatorInteger(),
      'sector_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Sector'))),
      'tiposector_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoSector'))),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('sector_criadero[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SectorCriadero';
  }

}
