<?php

/**
 * Jaula form base class.
 *
 * @method Jaula getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseJaulaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'codigo'            => new sfWidgetFormInputText(),
      'columna'           => new sfWidgetFormInputText(),
      'fila'              => new sfWidgetFormInputText(),
      'sectorcriadero_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SectorCriadero'), 'add_empty' => false)),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'codigo'            => new sfValidatorString(array('max_length' => 255)),
      'columna'           => new sfValidatorInteger(),
      'fila'              => new sfValidatorInteger(),
      'sectorcriadero_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SectorCriadero'))),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('jaula[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Jaula';
  }

}
