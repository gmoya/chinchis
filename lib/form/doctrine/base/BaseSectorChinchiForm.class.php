<?php

/**
 * SectorChinchi form base class.
 *
 * @method SectorChinchi getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSectorChinchiForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'alias'           => new sfWidgetFormInputText(),
      'chinchillero_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchillero'), 'add_empty' => false)),
      'sector_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sector'), 'add_empty' => false)),
      'tiposector_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoSector'), 'add_empty' => false)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'alias'           => new sfValidatorString(array('max_length' => 255)),
      'chinchillero_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchillero'))),
      'sector_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Sector'))),
      'tiposector_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoSector'))),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('sector_chinchi[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SectorChinchi';
  }

}
