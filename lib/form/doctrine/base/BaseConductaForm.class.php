<?php

/**
 * Conducta form base class.
 *
 * @method Conducta getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseConductaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'actmaternal'    => new sfWidgetFormInputText(),
      'otros'          => new sfWidgetFormInputText(),
      'seguimiento_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Seguimiento'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'actmaternal'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'otros'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'seguimiento_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Seguimiento'))),
    ));

    $this->widgetSchema->setNameFormat('conducta[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Conducta';
  }

}
