<?php

/**
 * Fallecimiento form base class.
 *
 * @method Fallecimiento getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFallecimientoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'chinchilla_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'), 'add_empty' => false)),
      'fallecimiento_at' => new sfWidgetFormDateTime(),
      'causa_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CausaFallecimiento'), 'add_empty' => false)),
      'otros'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'chinchilla_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'))),
      'fallecimiento_at' => new sfValidatorDateTime(),
      'causa_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CausaFallecimiento'))),
      'otros'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fallecimiento[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Fallecimiento';
  }

}
