<?php

/**
 * Chinchilla form base class.
 *
 * @method Chinchilla getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseChinchillaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'codigo'            => new sfWidgetFormInputText(),
      'reproductor'       => new sfWidgetFormInputCheckbox(),
      'fechanac'          => new sfWidgetFormDate(),
      'color_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Color'), 'add_empty' => false)),
      'sexo_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sexo'), 'add_empty' => false)),
      'criadero_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Criadero'), 'add_empty' => false)),
      'sectorcriadero_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SectorCriadero'), 'add_empty' => true)),
      'jaula_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Jaula'), 'add_empty' => true)),
      'fila'              => new sfWidgetFormInputText(),
      'usuario_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'codigo'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'reproductor'       => new sfValidatorBoolean(array('required' => false)),
      'fechanac'          => new sfValidatorDate(array('required' => false)),
      'color_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Color'))),
      'sexo_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Sexo'))),
      'criadero_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Criadero'))),
      'sectorcriadero_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SectorCriadero'), 'required' => false)),
      'jaula_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Jaula'), 'required' => false)),
      'fila'              => new sfValidatorInteger(array('required' => false)),
      'usuario_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('chinchilla[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Chinchilla';
  }

}
