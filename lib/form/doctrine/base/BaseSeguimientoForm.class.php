<?php

/**
 * Seguimiento form base class.
 *
 * @method Seguimiento getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSeguimientoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'tiposeguimiento_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoSeguimiento'), 'add_empty' => false)),
      'chinchilla_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'), 'add_empty' => false)),
      'observaciones'      => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'tiposeguimiento_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoSeguimiento'))),
      'chinchilla_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'))),
      'observaciones'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('seguimiento[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Seguimiento';
  }

}
