<?php

/**
 * Nacimiento form base class.
 *
 * @method Nacimiento getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseNacimientoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'fecha_nac'     => new sfWidgetFormDateTime(),
      'chinchilla_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'), 'add_empty' => true)),
      'parimiento_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Parimiento'), 'add_empty' => false)),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'fecha_nac'     => new sfValidatorDateTime(),
      'chinchilla_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'), 'required' => false)),
      'parimiento_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Parimiento'))),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('nacimiento[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Nacimiento';
  }

}
