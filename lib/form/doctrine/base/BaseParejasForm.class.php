<?php

/**
 * Parejas form base class.
 *
 * @method Parejas getObject() Returns the current form's model object
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseParejasForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'macho_id'   => new sfWidgetFormInputText(),
      'hembra_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'), 'add_empty' => false)),
      'desde_at'   => new sfWidgetFormDateTime(),
      'hasta_at'   => new sfWidgetFormDateTime(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'macho_id'   => new sfValidatorInteger(),
      'hembra_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Chinchilla'))),
      'desde_at'   => new sfValidatorDateTime(),
      'hasta_at'   => new sfValidatorDateTime(array('required' => false)),
      'created_at' => new sfValidatorDateTime(),
      'updated_at' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('parejas[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Parejas';
  }

}
