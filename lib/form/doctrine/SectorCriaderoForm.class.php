<?php

/**
 * SectorCriadero form.
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SectorCriaderoForm extends BaseSectorCriaderoForm
{
  public function configure()
  {
		unset($this['created_at'], $this['updated_at']);
		
		$this->setWidget('criadero_id', new sfWidgetFormInputHidden());
		$this->widgetSchema['tiposector_id']->setLabel('Tipo de sector');
  }

	public function personalizar($criadero_id)
	{
		$this->setDefault('criadero_id', $criadero_id);
		$choices = Doctrine_Core::getTable('SectorCriadero')->getSectoresFaltantes($criadero_id);
		$this->widgetSchema['sector_id'] = new sfWidgetFormChoice(array('choices' => $choices));
	}

	public function doSave($con = null)
	{
		$valores = $this->getValues();
		parent::doSave($con);
		$this->verificarJaulas($valores);
	}	

	public function verificarJaulas($valores)
	{
		$sectorc_id = $this->getObject()->getId();

		for ($f=1; $f<=$valores['filas']; $f++)
		{
				for ($c=1; $c<=$valores['columnas']; $c++)
				{
						if (!Doctrine::getTable('Jaula')->existe($sectorc_id, $f, $c))
						{
								$jaula = new Jaula();
								$jaula->setCodigo($valores['sector_id'].$f.$c);
								$jaula->setColumna($c);
								$jaula->setFila($f);
								$jaula->setSectorcriaderoId($sectorc_id);
								$jaula->save();
								unset($jaula);
						}
				}
		}

	}
}
