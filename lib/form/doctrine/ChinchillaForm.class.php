<?php

/**
 * Chinchilla form.
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ChinchillaForm extends BaseChinchillaForm
{
  public function configure()
  {
		unset($this['created_at'], $this['updated_at'], $this['criadero_id'],
			$this['jaula_id'], $this['sectorcriadero_id'], $this['fila']
		);

		$this->widgetSchema['usuario_id'] = new sfWidgetFormInputHidden();

/**	Se comenta para hacer más sencillo el alta

	    $this->widgetSchema['jaula_id']->setOption('add_empty', true);

		$this->widgetSchema['criadero_id'] = new sfWidgetFormDoctrineChoice(array(
    	'model' 		=> 'Criadero',
			'add_empty'	=> 'Seleccione criadero',
#	    'method' => '__toString',
#			'with'	=> 'nuevo='.$this->getObject()->isNew(),
#  	  'url' => 'localidades/elegirPorProvincia',
#			'update' => 'chinchilla_sectorcriadero_id',
#			'update_element' => 'select',
#			'add_empty'=>true
  	));

		$this->widgetSchema['sectorcriadero_id'] = new sfWidgetFormDoctrineDependentSelect(array(
			'model'			=> 'Sectorcriadero',
			'depends'		=> 'Criadero',
			'add_empty'	=> 'Seleccione sector',
			'ajax'			=> true,
			'order_by'	=> array('alias', 'asc'),
			'ref_method' => 'getCriaderoId'
		));

		$this->widgetSchema['jaula_id'] = new sfWidgetFormDoctrineDependentSelect(array(
			'model'	=> 'Jaula',
			'depends'	=> 'Sectorcriadero',
			'add_empty'	=> 'Seleccione jaula',
			'ajax'			=> true,
			'order_by'	=> array('codigo', 'asc')
		));
*/
  }

	public function personalizar($usuario_id)
	{
		$this->setDefault('usuario_id', $usuario_id);
	}
}
