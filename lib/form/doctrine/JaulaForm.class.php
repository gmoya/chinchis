<?php

/**
 * Jaula form.
 *
 * @package    chinchis
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class JaulaForm extends BaseJaulaForm
{
  public function configure()
  {
		unset($this['created_at'], $this['updated_at'], $this['codigo'], 
					$this['columna'], $this['fila'], $this['sectorcriadero_id']);
		
		$this->widgetSchema['chinchi_id'] = new sfWidgetFormChoice(array('choices' => array()));
    $this->validatorSchema['chinchi_id'] = new sfValidatorPass();

    $this->validatorSchema->setOption('allow_extra_fields', true);
  }

	public function agregarChinchis($choices)
	{
		$this->widgetSchema['chinchi_id'] = new sfWidgetFormChoice(array('choices' => $choices));
	}

  public function doSave($con = null)
  {
    if (is_null($con))
    {
      $con = $this->getConnection();
    }

    $this->updateObject();

		$chinchi = Doctrine::getTable('Chinchilla')->find($this->getValue('chinchi_id'));
		$chinchi->setJaulaId($this->getObject()->getId());
		$chinchi->save();
		
    $this->object->save($con);

    return $this->object;
  }
}
