<?php

require_once dirname(__FILE__).'/../lib/criaderoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/criaderoGeneratorHelper.class.php';

/**
 * criadero actions.
 *
 * @package    chinchis
 * @subpackage criadero
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class criaderoActions extends autoCriaderoActions
{
	public function executeNew(sfWebRequest $request)
	{
		$this->form = $this->configuration->getForm();
		$this->form->personalizar($this->getUser()->getGuardUser());
    $this->criadero = $this->form->getObject();
		$this->ajax = $this->isAjax();
	}

  public function executeCreate(sfWebRequest $request)
  {
    $this->form = $this->configuration->getForm();
    $this->criadero = $this->form->getObject();
		$this->ajax = $this->isAjax();
   	$process = $this->processForm($request, $this->form);

    if ($this->ajax && $process) 
		{
			return $this->renderPartial('criadero/notice', array('notice' => $process['notice'], 'criadero' => $process['criadero']));
		}

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->criadero = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->criadero);
		$this->ajax = $this->isAjax();
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->criadero = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->criadero);
		$this->ajax = $this->isAjax();

   	$process = $this->processForm($request, $this->form);

    if ($this->getRequest()->isXmlHttpRequest() && $process) 
		{
			return $this->renderPartial('criadero/notice', array('notice' => $process['notice'], 'criadero' => $process['criadero']));
		}

    $this->setTemplate('edit');
  }

	public function executeAjaxAddCriadero(sfWebRequest $request)
	{
    $criadero = $this->getRoute()->getObject();
    $helper = new criaderoGeneratorHelper();
		
		return $this->renderPartial('criadero/bloque_criadero', array('criadero' => $criadero, 'helper' => $helper));
	}

  public function executeAjaxDelete(sfWebRequest $request)
  {
    $criadero = $this->getRoute()->getObject();
    $usuario_id = $this->getUser()->getId();
    $propietario = Doctrine_Core::getTable('Criadero')->verificarPropietario($criadero->getId(), $usuario_id);

    if (!$propietario || !$criadero)
    {
        $this->forward404Unless(false);
    }


    if ($this->isAjax() && $propietario)
    {
        $criadero->delete();

        return true;
    }
  }

	public function isAjax()
	{
		return $this->getRequest()->isXmlHttpRequest();
	}

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
		$ajax = $this->isAjax();

    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $criadero = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $criadero)));

	    if ($ajax) {
  			return array('notice' => $notice, 'criadero' => $criadero);
      }
      elseif ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@criadero_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect(array('sf_route' => 'criadero_edit', 'sf_subject' => $criadero));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

}
