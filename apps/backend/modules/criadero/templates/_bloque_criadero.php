<?php use_helper('Date') ?>
<table cellspacing="0" id="criadero_<?php echo $criadero->getId() ?>" class="bloque_criadero">
  <thead>
    <tr>
      <th id="criadero_<?php echo $criadero->getId() ?>_nombre_th">
				<span id="criadero_<?php echo $criadero->getId() ?>_nombre" class="criadero-nombre"><?php echo $criadero->getNombre() ?></span> 
      	<?php include_partial('criadero/list_td_actions', array('criadero' => $criadero, 'helper' => $helper)) ?>
			</th>
    </tr>
  </thead>
	<tfoot>
		<tr>
			<td colspan=3><?php echo 'Creado el '.format_date($criadero->getCreatedAt(), 'D', 'es') ?></td>
		</tr>
	</tfoot>
  <tbody id="criadero_<?php echo $criadero->getId() ?>_sectores">
		<?php include_partial('criadero/sectores', array('criadero' => $criadero, 'sectores_criadero' => $criadero->getSectorCriadero(), 'helper' => $helper)) ?>
  </tbody>
</table>
