<?php use_helper('I18N') ?>
<script>
  jQuery(document).ready(function() {
		jQuery.colorbox.resize();
    update = jQuery("#criadero_<?php echo $criadero->getId() ?>").length;
    if (update) {
			jQuery("#criadero_<?php echo $criadero->getId() ?>_nombre").html("<?php echo $criadero->getNombre() ?>");
		} else {
	    jQuery.ajax({
  	    type:'GET',
    	  dataType:'html',
    	  data:jQuery(this).serialize(),
      	success:function(data, textStatus){
        	jQuery("#criaderos").append(data);
					jQuery("#criadero_<?php echo $criadero->getId() ?> .colorbox").colorbox();
      },
      url:"<?php echo url_for('criadero/ajaxAddCriadero?id='.$criadero->getId()) ?>"
    });
		setTimeout(function(){ jQuery.colorbox.close() }, 2000);
    return false;
		}
  });
</script>

<p><?php echo __($notice, array(), 'sf_admin') ?></p>
