<?php use_helper('I18N', 'Date') ?>
<?php include_partial('criadero/assets') ?>

<div id="sf_admin_container">
  <h1>
	<ul class="sf_admin_actions flotante" id="criadero_new">
	<?php echo __('Mis Criaderos', array(), 'messages').' ' ?>
	<?php echo $helper->linkToNewAjax(array(  'params' =>   array(  ),  'class_suffix' => '',  'label' => ' ',))?>
	</ul>
	</h1>
  <?php include_partial('criadero/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('criadero/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">
    <?php #include_partial('criadero/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('criadero/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('criadero/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('criadero/list_actions', array('helper' => $helper)) ?>
    </ul>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('criadero/list_footer', array('pager' => $pager)) ?>
  </div>
</div>

