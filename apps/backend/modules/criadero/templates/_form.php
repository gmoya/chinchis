<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
	<?php if ($ajax) : ?>
  	<?php use_helper('jQuery') ?>
 		<script>
		  jQuery(document).ready(function() {
    		jQuery.colorbox.resize();
			});
		</script>
   <?php $url = $form->isNew() ? 'criadero' : url_for_form($form, '@criadero') ?>
    <?php echo jq_form_remote_tag(array('update' => 'sf_admin_container', 'url' => $url), array('method' => 'POST')) ?>
    <?php if (!$form->isNew()) : ?>
      <input type="hidden" value="put" name="sf_method">
    <?php endif ?>
  <?php else : ?>
		<?php echo form_tag_for($form, '@criadero') ?>
  <?php endif ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
      <?php include_partial('criadero/form_fieldset', array('criadero' => $criadero, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
    <?php endforeach; ?>

    <?php include_partial('criadero/form_actions', array('criadero' => $criadero, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper, 'ajax' => $ajax)) ?>
  </form>
</div>
