<div class="sf_admin_list">
  <?php if (!$pager->getNbResults()): ?>
    <p><?php echo __('No result', array(), 'sf_admin') ?></p>
  <?php else: ?>
		<div id="criaderos">
			<?php foreach ($pager->getResults() as $i => $criadero): $odd = fmod(++$i, 2) ? 'odd' : 'even' ?>
				<?php include_partial('criadero/bloque_criadero', array('criadero' => $criadero, 'helper' => $helper)) ?>
			<?php endforeach; ?>
		</div>
  <?php endif; ?>
</div>
