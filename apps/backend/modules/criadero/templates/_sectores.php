<?php use_helper('I18N') ?>
	<tr>
		<th>Sector  
			<ul id="sectorcriadero_new" class="sf_admin_actions flotante">
				<?php echo $helper->linkToNewSectorCriaderoAjax(array('params' => array(), 'class_suffix' => 'new', 'label' => ' '), $criadero) ?>
			</ul>
		<th>Tipo de Sector
		<th>Acciones
	</tr>
	<tr class="sectores_vacios" <?php if (count($sectores_criadero) > 0) { echo("style='display:none'"); } ?>>
		<td colspan=3> <p>No hay sectores.</p>
	</tr>
	<?php foreach ($sectores_criadero as $sector_criadero) : ?>
		<?php include_partial('criadero/sector', array('sc' => $sector_criadero, 'helper' => $helper, 'update' => false)) ?>
	<?php endforeach ?>
