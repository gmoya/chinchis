<?php if (!$update) : ?>
<tr id="sectorcriadero_<?php echo $sc->getId() ?>">
<?php endif ?>
	<td><a href="<?php echo url_for('sectorcriadero/show?id='.$sc->getId()) ?>"><?php echo $sc->getSectorId().' - '.$sc->getAlias() ?></td>
	<td><?php echo $sc->getTipoSector() ?></td>
	<td>
	  <ul class="sf_admin_td_actions">
		 	<?php echo $helper->linkToEditSectorCriaderoAjax($sc, array('params' => array(), 'class_suffix' => 'edit', 'label' => ' ')) ?>
	  	<?php echo $helper->linkToDeleteSectorCriaderoAjax($sc, array('params' => array(), 'confirm' => 'Are you sure?', 'class_suffix' => 'delete', 'label' => ' ')) ?>
	 	</ul>
	</th>
<?php if (!$update) : ?>
</tr>
<?php endif ?>
