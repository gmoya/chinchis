<?php

/**
 * sectorcriadero module helper.
 *
 * @package    chinchis
 * @subpackage sectorcriadero
 * @author     Your name here
 * @version    SVN: $Id: helper.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sectorcriaderoGeneratorHelper extends BaseSectorcriaderoGeneratorHelper
{
	public function linkToNewSectorCriaderoAjax($params, $criadero)
	{
		return '<li class="sf_admin_action_new"><a class="colorbox" title="Nuevo" href="'.url_for('sectorcriadero/new?criadero_id='.$criadero->getId()).'">'.__($params['label'], array(), 'sf_admin').'</li>';
	}

	public function linkToEditSectorCriaderoAjax($object, $params)
  {
    return '<li class="sf_admin_action_edit"><a id="sectorcriadero_'.$object->getId().'_edit" class="colorbox" title="Editar" href="'.url_for('sectorcriadero/edit?id='.$object->getId()).'"> </a></li>';
  }

  public function linkToDeleteSectorCriaderoAjax($object, $params)
  {
    if ($object->isNew())
    {
      return '';
    }

		use_helper('jQuery');

		return '<li class="sf_admin_action_delete">'.jq_link_to_remote(' ',
              array(
                'url'     =>  url_for('sectorcriadero/deleteMe?im='.$object->getId()),
                'success' =>  "jQuery('#sectorcriadero_".$object->getId()."').remove();",
                'confirm' => '¿Esta seguro?'
              ),
              array(
                'class' => 'sectorcriadero_actions sectorcriadero_delete',
                'title' => 'Eliminar'
              )
          ).'</li>';
  }

}
