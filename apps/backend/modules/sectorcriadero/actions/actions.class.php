<?php

require_once dirname(__FILE__).'/../lib/sectorcriaderoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sectorcriaderoGeneratorHelper.class.php';

/**
 * sectorcriadero actions.
 *
 * @package    chinchis
 * @subpackage sectorcriadero
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sectorcriaderoActions extends autoSectorcriaderoActions
{
	public function executeShow(sfWebRequest $request)
	{
		$this->sc = $this->getRoute()->getObject();
		$usuarioId = $this->getUser()->getId();
		$propietario = Doctrine_Core::getTable('Criadero')->verificarPropietario($this->sc->getCriaderoId(), $usuarioId);
		
		if (!$propietario)
		{
				$this->forward404Unless(false);
		}
	}
	
	# Verifica que el criadero pertenezca al usuario y lo setea al formulario

  public function executeNew(sfWebRequest $request)
  {
		$criadero_id = (int) $request->getParameter('criadero_id');
		$usuario_id = $this->getUser()->getId();
		$propietario = Doctrine_Core::getTable('Criadero')->verificarPropietario($criadero_id, $usuario_id);
		
		if (!$propietario || !$criadero_id)
		{
				$this->forward404Unless(false);
		}

	  $this->form = $this->configuration->getForm();
		$this->form->personalizar($criadero_id);
	  
		$this->sectorcriadero = $this->form->getObject();
    $this->ajax = $this->isAjax();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->form = $this->configuration->getForm();
    $this->sectorcriadero = $this->form->getObject();
    $this->ajax = $this->isAjax();

    $process = $this->processForm($request, $this->form);

    if ($this->ajax && $process) 
		{
			return $this->renderPartial('sectorcriadero/notice', array('notice' => $process['notice'], 'sc' => $process['sectorcriadero']));
		}

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->sectorcriadero = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->sectorcriadero);
    $this->ajax = $this->isAjax();
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->sectorcriadero = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->sectorcriadero);
    $this->ajax = $this->isAjax();

    $process = $this->processForm($request, $this->form);

    if ($this->ajax && $process) 
		{
			return $this->renderPartial('sectorcriadero/notice', array('notice' => $process['notice'], 'sc' => $process['sectorcriadero']));
		}
    
		$this->setTemplate('edit');
  }

	public function executeAjaxSectorcriadero(sfWebRequest $request)
	{
		$sc = $this->getRoute()->getObject();
		$update = $request->getParameter('update') || false;

		return $this->renderPartial('criadero/sector', array('sc' => $sc, 'update' => $update, 'helper' => new SectorcriaderoGeneratorHelper()));
	}

	public function executeAjaxDelete(sfWebRequest $request)
  {
    $sc = $this->getRoute()->getObject();
    $usuario_id = $this->getUser()->getId();
    $propietario = Doctrine_Core::getTable('Criadero')->verificarPropietario($sc->getCriaderoId(), $usuario_id);

    if (!$propietario || !$sc)
    {
        $this->forward404Unless(false);
    }


    if ($this->isAjax() && $propietario)
    {
        $sc->delete();

        return true;
    }
	}

	public function isAjax()
	{
		return $this->getRequest()->isXmlHttpRequest();
	}

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $ajax = $this->isAjax();

    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $sectorcriadero = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $sectorcriadero)));

	    if ($ajax) {
  			return array('notice' => $notice, 'sectorcriadero' => $sectorcriadero);
      }
			elseif ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@sectorcriadero_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect(array('sf_route' => 'sectorcriadero_edit', 'sf_subject' => $sectorcriadero));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
}
