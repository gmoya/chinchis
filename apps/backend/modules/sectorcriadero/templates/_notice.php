<?php use_helper('I18N') ?>
<script>
  jQuery(document).ready(function() {
		jQuery.colorbox.resize();
    update = jQuery("#sectorcriadero_<?php echo $sc->getId() ?>").length;
    destino = (update) ? "sectorcriadero/<?php echo $sc->getId() ?>/ajaxSectorcriadero?update=true" : "sectorcriadero/<?php echo $sc->getId() ?>/ajaxSectorcriadero";
		vacios = jQuery("#criadero_<?php echo $sc->getCriaderoId() ?> .sectores_vacios");
    jQuery.ajax({
      type:'GET',
      dataType:'html',
      data:jQuery(this).serialize(),
      success:function(data, textStatus){
        (update) ? jQuery("#sectorcriadero_<?php echo $sc->getId() ?>").html(data) : jQuery('#criadero_<?php echo $sc->getCriaderoId() ?>_sectores').append(data);
				(vacios.length) ? vacios.fadeOut('slow') : '';
        jQuery("#sectorcriadero_<?php echo $sc->getId() ?>_edit").colorbox();
      },
      url:destino
    });
		setTimeout(function(){ jQuery.colorbox.close() }, 2000);
    return false;
  });
</script>

<p><?php echo __($notice, array(), 'sf_admin') ?></p>
