<?php use_stylesheet('/sfDoctrinePlugin/css/global.css') ?>
<?php use_stylesheet('/sfDoctrinePlugin/css/default.css') ?>
<div id="sf_admin_container">
	<div class="sectorcriadero">
  	<h1>Sector <?php echo $sc->getSectorId().' - '.$sc->getAlias() ?></h1>
	</div>

	<?php $jaulas = $sc->getJaula() ?>
	<?php $contadorJaulas = 0 ?>
	<div id="sf_admin_content">
		<div class="sf_admin_list">
			<table class="jaulas">
				<thead>	<tr><th colspan="<?php echo $sc->isCria() ? $sc->getColumnas()+1 : $sc->getColumnas() ?>">Jaulas</th></tr>	</thead>
				<tbody>
				<?php for ($fila=1; $fila<=$sc->getFilas(); $fila++) : ?>
 			   	<tr class="<?php echo fmod($fila, 2) ? 'even' : 'odd' ?>">
					<?php if ($sc->isCria()) : ?>
						<td>
								<?php if(count($macho = $sc->hasMachoFila($fila))) : ?>
									<?php echo $macho ?>
								<?php else : ?>
									<p><a href="<?php echo url_for('sectorcriadero/insertarMacho?id='.$sc->getId()).'?fila='.$fila ?>" class="colorbox"><?php echo ('Sin macho') ?></a></p>
								<?php endif ?>
						</td>
						<?php endif ?>

					<?php for ($columna=1; $columna<=$sc->getColumnas(); $columna++) : ?>
      			<td class="location">
        			<div>
								<p><?php echo $jaulas[$contadorJaulas]->getCodigo() ?></p>
								<?php $chinchi = $jaulas[$contadorJaulas]->getChinchilla() ?>
								<?php if (count($chinchi)) : ?>
									<p><?php echo $chinchi[0]->getCodigo() ?></p>
								<?php else : ?>
									<p><a href="<?php echo url_for('jaula/edit?id='.$jaulas[$contadorJaulas]->getId()) ?>" class="colorbox"><?php echo ("vacio") ?></a></p>
								<?php endif ?>
      			</td>
						<?php $contadorJaulas++ ?>
					<?php endfor ?>	
    			</tr>
				<?php endfor ?>	
				</tbody>
			</table>
		</div>
	</div>
	<ul class="sf_admin_actions">
		<li class="sf_admin_action_list"><a href="<?php echo url_for('@criadero') ?>">Mis Criaderos</a></li>
	</ul>
</div>
