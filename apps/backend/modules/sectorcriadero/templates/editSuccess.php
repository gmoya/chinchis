<?php use_helper('I18N', 'Date') ?>
<?php include_partial('sectorcriadero/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit Sectorcriadero', array(), 'messages') ?></h1>

  <?php include_partial('sectorcriadero/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('sectorcriadero/form_header', array('sectorcriadero' => $sectorcriadero, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('sectorcriadero/form', array('sectorcriadero' => $sectorcriadero, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper, 'ajax' => $ajax)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('sectorcriadero/form_footer', array('sectorcriadero' => $sectorcriadero, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
