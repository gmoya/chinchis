<?php $attributes = array() ?>
<?php $class = array() ?>
<?php $label = array() ?>

<fieldset id="sf_fieldset_<?php echo preg_replace('/[^a-z0-9_]/', '_', strtolower($fieldset)) ?>">
  <?php if ('NONE' != $fieldset): ?>
    <h2><?php echo __($fieldset, array(), 'messages') ?></h2>
  <?php endif ?>
  
	<?php foreach ($fields as $name => $field): ?>
    <?php if ((isset($form[$name]) && $form[$name]->isHidden()) || (!isset($form[$name]) && $field->isReal())) continue ?>
    <?php 
      $attributes[$name] = $field->getConfig('attributes', array());
      $label[$name]      = $field->getConfig('label');
      $help[$name]       = $field->getConfig('help');
      $class[$name]      = 'sf_admin_form_row sf_admin_'.strtolower($field->getType()).' sf_admin_form_field_'.$name;
   ?>
  <div class="<?php echo $class[$name] ?><?php $form[$name]->hasError() and print ' errors' ?>">
    <?php echo $form[$name]->renderError() ?>
    <div>
      <?php echo $form[$name]->renderLabel($label[$name]) ?>

      <div class="content"><?php echo $form[$name]->render($attributes[$name] instanceof sfOutputEscaper ? $attributes[$name]->getRawValue() : $attributes[$name]) ?></div>

      <?php if ($help[$name]): ?>
        <div class="help"><?php echo __($help[$name], array(), 'messages') ?></div>
      <?php elseif ($help['codigo'] = $form[$name]->renderHelp()): ?>
        <div class="help"><?php echo $help[$name] ?></div>
      <?php endif; ?>
    </div>
  </div>

  <?php endforeach ?>
<?php /*
  <div class="<?php echo $class['codigo'] ?><?php $form['codigo']->hasError() and print ' errors' ?>">
    <?php echo $form['codigo']->renderError() ?>
    <div>
      <?php echo $form['codigo']->renderLabel($label['codigo']) ?>

      <div class="content"><?php echo $form['codigo']->render($attributes['codigo'] instanceof sfOutputEscaper ? $attributes['codigo']->getRawValue() : $attributes['codigo']) ?></div>

      <?php if ($help['codigo']): ?>
        <div class="help"><?php echo __($help['codigo'], array(), 'messages') ?></div>
      <?php elseif ($help['codigo'] = $form['codigo']->renderHelp()): ?>
        <div class="help"><?php echo $help['codigo'] ?></div>
      <?php endif; ?>
    </div>
  </div>

  <div class="<?php echo $class['reproductor'] ?><?php $form['reproductor']->hasError() and print ' errors' ?>">
    <?php echo $form['reproductor']->renderError() ?>
    <div>
      <?php echo $form['reproductor']->renderLabel($label['reproductor']) ?>

      <div class="content"><?php echo $form['reproductor']->render($attributes['reproductor'] instanceof sfOutputEscaper ? $attributes['reproductor']->getRawValue() : $attributes['reproductor']) ?></div>

      <?php if ($help['reproductor']): ?>
        <div class="help"><?php echo __($help['reproductor'], array(), 'messages') ?></div>
      <?php elseif ($help['reproductor'] = $form['reproductor']->renderHelp()): ?>
        <div class="help"><?php echo $help['reproductor'] ?></div>
      <?php endif; ?>
    </div>
  </div>
  <div class="<?php echo $class['color_id'] ?><?php $form['color_id']->hasError() and print ' errors' ?>">
    <?php echo $form['color_id']->renderError() ?>
    <div>
      <?php echo $form['color_id']->renderLabel($label['color_id']) ?>

      <div class="content"><?php echo $form['color_id']->render($attributes['color_id'] instanceof sfOutputEscaper ? $attributes['color_id']->getRawValue() : $attributes['color_id']) ?></div>

      <?php if ($help['color_id']): ?>
        <div class="help"><?php echo __($help['color_id'], array(), 'messages') ?></div>
      <?php elseif ($help['color_id'] = $form['color_id']->renderHelp()): ?>
        <div class="help"><?php echo $help['color_id'] ?></div>
      <?php endif; ?>
    </div>
  </div>
  <div class="<?php echo $class['sexo_id'] ?><?php $form['sexo_id']->hasError() and print ' errors' ?>">
    <?php echo $form['sexo_id']->renderError() ?>
    <div>
      <?php echo $form['sexo_id']->renderLabel($label['sexo_id']) ?>

      <div class="content"><?php echo $form['sexo_id']->render($attributes['sexo_id'] instanceof sfOutputEscaper ? $attributes['sexo_id']->getRawValue() : $attributes['sexo_id']) ?></div>

      <?php if ($help['sexo_id']): ?>
        <div class="help"><?php echo __($help['sexo_id'], array(), 'messages') ?></div>
      <?php elseif ($help['sexo_id'] = $form['sexo_id']->renderHelp()): ?>
        <div class="help"><?php echo $help['sexo_id'] ?></div>
      <?php endif; ?>
    </div>
  </div>
  <div class="<?php echo $class['criadero_id'] ?><?php $form['criadero_id']->hasError() and print ' errors' ?>">
    <?php echo $form['criadero_id']->renderError() ?>
    <div>
      <?php echo $form['criadero_id']->renderLabel($label['criadero_id']) ?>

      <div class="content"><?php echo $form['criadero_id']->render($attributes['criadero_id'] instanceof sfOutputEscaper ? $attributes['criadero_id']->getRawValue() : $attributes['criadero_id']) ?></div>

      <?php if ($help['criadero_id']): ?>
        <div class="help"><?php echo __($help['criadero_id'], array(), 'messages') ?></div>
      <?php elseif ($help['criadero_id'] = $form['criadero_id']->renderHelp()): ?>
        <div class="help"><?php echo $help['criadero_id'] ?></div>
      <?php endif; ?>
    </div>
  </div>
  <div class="<?php echo $class['sectorcriadero_id'] ?><?php $form['sectorcriadero_id']->hasError() and print ' errors' ?>">
    <?php echo $form['sectorcriadero_id']->renderError() ?>
    <div>
      <?php echo $form['sectorcriadero_id']->renderLabel($label['sectorcriadero_id']) ?>

      <div class="content"><?php echo $form['sectorcriadero_id']->render($attributes['sectorcriadero_id'] instanceof sfOutputEscaper ? $attributes['sectorcriadero_id']->getRawValue() : $attributes['sectorcriadero_id']) ?></div>

      <?php if ($help['sectorcriadero_id']): ?>
        <div class="help"><?php echo __($help['sectorcriadero_id'], array(), 'messages') ?></div>
      <?php elseif ($help['sectorcriadero_id'] = $form['sectorcriadero_id']->renderHelp()): ?>
        <div class="help"><?php echo $help['sectorcriadero_id'] ?></div>
      <?php endif; ?>
    </div>
  </div>
  <div class="<?php echo $class['jaula_id'] ?><?php $form['jaula_id']->hasError() and print ' errors' ?>">
    <?php echo $form['jaula_id']->renderError() ?>
    <div>
      <?php echo $form['jaula_id']->renderLabel($label['jaula_id']) ?>

      <div class="content"><?php echo $form['jaula_id']->render($attributes['jaula_id'] instanceof sfOutputEscaper ? $attributes['jaula_id']->getRawValue() : $attributes['jaula_id']) ?></div>

      <?php if ($help['jaula_id']): ?>
        <div class="help"><?php echo __($help['jaula_id'], array(), 'messages') ?></div>
      <?php elseif ($help['jaula_id'] = $form['jaula_id']->renderHelp()): ?>
        <div class="help"><?php echo $help['jaula_id'] ?></div>
      <?php endif; ?>
    </div>
  </div>
*/?>
</fieldset>
