<?php use_stylesheet('/sfDoctrinePlugin/css/global.css') ?>
<?php use_stylesheet('/sfDoctrinePlugin/css/default.css') ?>

<div id="sf_admin_container">
	<!--<>-->
	<div class="box">

		<div class="box-title chinchilla">
			<h1>Chinchilla <?php echo $chinchi->getCodigo() ?></h1>
		</div>

		<div class="box-content">
			<div class="box-data">
				<ul>
					<li><span class="strong">Fecha de Nacimiento:</span> <?php echo $chinchi->getFechanac()  ?></li>
					<li><span class="strong">Color:</span> <span class="strongblack"> <?php echo $chinchi->getColor() ?> </span> </li>
					<li><span class="strong">Sexo:</span> <?php echo $chinchi->getSexo() ?></li>
					<li><span class="strong">Reproductor:</span> <?php echo $chinchi->getReproductor() ? "si" : "no" ?></li>

				</ul>
			</div>
		</div>
	</div>

	<ul class="sf_admin_actions">
		<li class="sf_admin_action_list"><a href="<?php echo url_for('@criadero') ?>">Mis Criaderos</a></li>
	</ul>

</div>