<?php

require_once dirname(__FILE__).'/../lib/chinchillaGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/chinchillaGeneratorHelper.class.php';

/**
 * chinchilla actions.
 *
 * @package    chinchis
 * @subpackage chinchilla
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class chinchillaActions extends autoChinchillaActions
{
	public function executeNew(sfWebRequest $request)
	{
	 	$this->form = $this->configuration->getForm();
	  	$this->form->personalizar($this->getUser()->getId());
	 	$this->chinchilla = $this->form->getObject();
	}
	
	public function executeShow(sfWebRequest $request)
	{
	 	$this->chinchi = $this->getRoute()->getObject();
	}
}
