<?php

require_once dirname(__FILE__).'/../lib/jaulaGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/jaulaGeneratorHelper.class.php';

/**
 * jaula actions.
 *
 * @package    chinchis
 * @subpackage jaula
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class jaulaActions extends autoJaulaActions
{
	public function executeEdit(sfWebRequest $request)
	{
		$choices = Doctrine_Core::getTable('Chinchilla')->getChinchisDisponibles($this->getUser()->getId());
   	$this->jaula = $this->getRoute()->getObject();
   	$this->form = count($choices) ? $this->configuration->getForm($this->jaula) : null;
    $this->ajax = $this->isAjax();
		
		if ($this->form) $this->form->agregarChinchis($choices);
	}
	
	public function isAjax()
	{
		return $this->getRequest()->isXmlHttpRequest();
	}
}
