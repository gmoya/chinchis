<?php use_helper('I18N', 'Date') ?>
<?php include_partial('jaula/assets') ?>

<div id="sf_admin_container">
	<?php if (!$form) : ?>
	<p>No hay chinchillas disponibles.</p>
	<?php else : ?>
  <h1><?php echo __('Edit Cage', array(), 'messages').' '.$jaula ?></h1>

  <?php include_partial('jaula/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('jaula/form_header', array('jaula' => $jaula, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('jaula/form', array('jaula' => $jaula, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper, 'ajax' => $ajax)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('jaula/form_footer', array('jaula' => $jaula, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
	<?php endif ?>
</div>
