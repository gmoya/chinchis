<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
 	<?php if ($ajax) : ?>
  	<?php use_helper('jQuery') ?>
		<script>
		  jQuery(document).ready(function() {
    		jQuery.colorbox.resize();
			});
		</script>
    <?php $url = $form->isNew() ? 'jaula' : url_for_form($form, '@jaula') ?>
    <?php echo jq_form_remote_tag(array('update' => 'sf_admin_container', 'url' => $url), array('method' => 'POST')) ?>
    <?php if (!$form->isNew()) : ?>
      <input type="hidden" value="put" name="sf_method">
    <?php endif ?>
  <?php else : ?>
		<?php echo form_tag_for($form, '@jaula') ?>
  <?php endif ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
      <?php include_partial('jaula/form_fieldset', array('jaula' => $jaula, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
    <?php endforeach; ?>

    <?php include_partial('jaula/form_actions', array('jaula' => $jaula, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </form>
</div>
