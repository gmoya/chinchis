<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
		<script>
			jQuery(document).ready(function(){ 
				jQuery('.colorbox').colorbox();
			});
		</script>
  </head>
  <body class="<?php echo !$sf_user->isAuthenticated() ? 'index' : 'logged' ?>">
    <div id="container">
      <?php if ($sf_user->isAuthenticated()): ?>
      <div id="header">
      	<div class="menu-container">
        	<div id="logo"><a href="<?php echo url_for('@homepage') ?>"><img src="/images/chinchis.png" /></a></div>
          <div id="menu">
            <?php include_partial('global/menu') ?>
          </div>
          <div id="usuario">
      			<span id="username"><?php echo $sf_user->getUsername() ?></span>
						<span id="logout">
      				<a href="<?php echo url_for('@sf_guard_signout') ?>" title="Salir"> </a>
    				</span>
					</div>
				</div>
			</div>
      <?php endif ?>
			<div id="content">
		   <?php echo $sf_content ?>
			</div>
			<div id="footer">
			</div>
  </body>
</html>
