<ul class="sf-menu sf-navbar sf-js-enabled sf-shadow">
  <?php if ($sf_user->isAuthenticated()) : ?>
    <li class="criaderos"><a id="criaderos" class="menu_uno sf-with-ul webnav" href="<?php echo url_for('criadero/index') ?>">Criaderos</a></li>
    <li class="chinchis"><a id="chinchis" class="menu_uno sf-with-ul webnav" href="<?php echo url_for('chinchilla/index') ?>">Chinchillas</a></li>
		<?php if ($sf_user->isSuperAdmin()) : ?>
		<li><?php echo link_to('Usuarios', '@sf_guard_user') ?></li>
		<?php endif ?>
	<?php endif ?>
</ul>
