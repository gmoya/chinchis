<td class="sf_admin_text sf_admin_list_td_id">
  <?php echo link_to($sectorcriadero->getId(), 'sectorcriadero_edit', $sectorcriadero) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_alias">
  <?php echo $sectorcriadero->getAlias() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_criadero_id">
  <?php echo $sectorcriadero->getCriaderoId() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_filas">
  <?php echo $sectorcriadero->getFilas() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_columnas">
  <?php echo $sectorcriadero->getColumnas() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_sector_id">
  <?php echo $sectorcriadero->getSectorId() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_tiposector_id">
  <?php echo $sectorcriadero->getTiposectorId() ?>
</td>
<td class="sf_admin_date sf_admin_list_td_created_at">
  <?php echo false !== strtotime($sectorcriadero->getCreatedAt()) ? format_date($sectorcriadero->getCreatedAt(), "f") : '&nbsp;' ?>
</td>
<td class="sf_admin_date sf_admin_list_td_updated_at">
  <?php echo false !== strtotime($sectorcriadero->getUpdatedAt()) ? format_date($sectorcriadero->getUpdatedAt(), "f") : '&nbsp;' ?>
</td>
