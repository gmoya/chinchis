<?php

/**
 * sectorcriadero module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage sectorcriadero
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: helper.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSectorcriaderoGeneratorHelper extends sfModelGeneratorHelper
{
  public function getUrlForAction($action)
  {
    return 'list' == $action ? 'sectorcriadero' : 'sectorcriadero_'.$action;
  }
}
