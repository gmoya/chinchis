<?php

/**
 * chinchilla module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage chinchilla
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: configuration.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseChinchillaGeneratorConfiguration extends sfModelGeneratorConfiguration
{
  public function getActionsDefault()
  {
    return array();
  }

  public function getFormActions()
  {
    return array(  '_delete' => NULL,  '_list' => NULL,  '_save' => NULL,  '_save_and_add' => NULL,);
  }

  public function getNewActions()
  {
    return array();
  }

  public function getEditActions()
  {
    return array();
  }

  public function getListObjectActions()
  {
    return array(  '_edit' => NULL,  '_delete' => NULL,);
  }

  public function getListActions()
  {
    return array(  '_new' => NULL,);
  }

  public function getListBatchActions()
  {
    return array(  '_delete' => NULL,);
  }

  public function getListParams()
  {
    return '%%id%% - %%codigo%% - %%fechanac%% - %%_color%% - %%_sexo%% - %%reproductor%% - %%_criadero%% - %%_sector%% - %%_jaula%%';
  }

  public function getListLayout()
  {
    return 'tabular';
  }

  public function getListTitle()
  {
    return 'Chinchillas';
  }

  public function getEditTitle()
  {
    return 'Edit Chinchilla';
  }

  public function getNewTitle()
  {
    return 'New Chinchilla';
  }

  public function getFilterDisplay()
  {
    return array(  0 => 'id',  1 => 'codigo',  2 => 'fechanac',  3 => 'color_id',  4 => 'sexo_id',  5 => 'reproductor',  6 => 'criadero_id',  7 => 'sectorcriadero_id',  8 => 'jaula_id',);
  }

  public function getFormDisplay()
  {
    return array();
  }

  public function getEditDisplay()
  {
    return array();
  }

  public function getNewDisplay()
  {
    return array();
  }

  public function getListDisplay()
  {
    return array(  0 => 'id',  1 => 'codigo',  2 => 'fechanac',  3 => '_color',  4 => '_sexo',  5 => 'reproductor',  6 => '_criadero',  7 => '_sector',  8 => '_jaula',);
  }

  public function getFieldsDefault()
  {
    return array(
      'id' => array(  'is_link' => true,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',),
      'codigo' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Código',),
      'reproductor' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Boolean',),
      'fechanac' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Date',  'label' => 'Nacimiento',  'date_format' => 'dd-MM-y',),
      'color_id' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'ForeignKey',),
      'sexo_id' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'ForeignKey',),
      'criadero_id' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'ForeignKey',),
      'sectorcriadero_id' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'ForeignKey',),
      'jaula_id' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'ForeignKey',),
      'fila' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',),
      'usuario_id' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'ForeignKey',),
      'created_at' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Date',),
      'updated_at' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Date',),
    );
  }

  public function getFieldsList()
  {
    return array(
      'id' => array(),
      'codigo' => array(),
      'reproductor' => array(),
      'fechanac' => array(),
      'color_id' => array(),
      'sexo_id' => array(),
      'criadero_id' => array(),
      'sectorcriadero_id' => array(),
      'jaula_id' => array(),
      'fila' => array(),
      'usuario_id' => array(),
      'created_at' => array(),
      'updated_at' => array(),
    );
  }

  public function getFieldsFilter()
  {
    return array(
      'id' => array(),
      'codigo' => array(),
      'reproductor' => array(),
      'fechanac' => array(),
      'color_id' => array(),
      'sexo_id' => array(),
      'criadero_id' => array(),
      'sectorcriadero_id' => array(),
      'jaula_id' => array(),
      'fila' => array(),
      'usuario_id' => array(),
      'created_at' => array(),
      'updated_at' => array(),
    );
  }

  public function getFieldsForm()
  {
    return array(
      'id' => array(),
      'codigo' => array(),
      'reproductor' => array(),
      'fechanac' => array(),
      'color_id' => array(),
      'sexo_id' => array(),
      'criadero_id' => array(),
      'sectorcriadero_id' => array(),
      'jaula_id' => array(),
      'fila' => array(),
      'usuario_id' => array(),
      'created_at' => array(),
      'updated_at' => array(),
    );
  }

  public function getFieldsEdit()
  {
    return array(
      'id' => array(),
      'codigo' => array(),
      'reproductor' => array(),
      'fechanac' => array(),
      'color_id' => array(),
      'sexo_id' => array(),
      'criadero_id' => array(),
      'sectorcriadero_id' => array(),
      'jaula_id' => array(),
      'fila' => array(),
      'usuario_id' => array(),
      'created_at' => array(),
      'updated_at' => array(),
    );
  }

  public function getFieldsNew()
  {
    return array(
      'id' => array(),
      'codigo' => array(),
      'reproductor' => array(),
      'fechanac' => array(),
      'color_id' => array(),
      'sexo_id' => array(),
      'criadero_id' => array(),
      'sectorcriadero_id' => array(),
      'jaula_id' => array(),
      'fila' => array(),
      'usuario_id' => array(),
      'created_at' => array(),
      'updated_at' => array(),
    );
  }


  /**
   * Gets the form class name.
   *
   * @return string The form class name
   */
  public function getFormClass()
  {
    return 'chinchillaForm';
  }

  public function hasFilterForm()
  {
    return true;
  }

  /**
   * Gets the filter form class name
   *
   * @return string The filter form class name associated with this generator
   */
  public function getFilterFormClass()
  {
    return 'chinchillaFormFilter';
  }

  public function getPagerClass()
  {
    return 'sfDoctrinePager';
  }

  public function getPagerMaxPerPage()
  {
    return 20;
  }

  public function getDefaultSort()
  {
    return array(null, null);
  }

  public function getTableMethod()
  {
    return '';
  }

  public function getTableCountMethod()
  {
    return '';
  }
}
