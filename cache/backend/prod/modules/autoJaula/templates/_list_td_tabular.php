<td class="sf_admin_text sf_admin_list_td_id">
  <?php echo link_to($jaula->getId(), 'jaula_edit', $jaula) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_codigo">
  <?php echo $jaula->getCodigo() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_columna">
  <?php echo $jaula->getColumna() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_fila">
  <?php echo $jaula->getFila() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_sectorcriadero_id">
  <?php echo $jaula->getSectorcriaderoId() ?>
</td>
<td class="sf_admin_date sf_admin_list_td_created_at">
  <?php echo false !== strtotime($jaula->getCreatedAt()) ? format_date($jaula->getCreatedAt(), "f") : '&nbsp;' ?>
</td>
<td class="sf_admin_date sf_admin_list_td_updated_at">
  <?php echo false !== strtotime($jaula->getUpdatedAt()) ? format_date($jaula->getUpdatedAt(), "f") : '&nbsp;' ?>
</td>
