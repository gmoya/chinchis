<td colspan="5">
  <?php echo __('%%id%% - %%nombre%% - %%usuario_id%% - %%created_at%% - %%updated_at%%', array('%%id%%' => link_to($criadero->getId(), 'criadero_edit', $criadero), '%%nombre%%' => $criadero->getNombre(), '%%usuario_id%%' => $criadero->getUsuarioId(), '%%created_at%%' => false !== strtotime($criadero->getCreatedAt()) ? format_date($criadero->getCreatedAt(), "f") : '&nbsp;', '%%updated_at%%' => false !== strtotime($criadero->getUpdatedAt()) ? format_date($criadero->getUpdatedAt(), "f") : '&nbsp;'), 'messages') ?>
</td>
