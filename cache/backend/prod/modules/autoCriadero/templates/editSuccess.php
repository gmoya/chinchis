<?php use_helper('I18N', 'Date') ?>
<?php include_partial('criadero/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Editar Criadero', array(), 'messages') ?></h1>

  <?php include_partial('criadero/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('criadero/form_header', array('criadero' => $criadero, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('criadero/form', array('criadero' => $criadero, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('criadero/form_footer', array('criadero' => $criadero, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
