<td class="sf_admin_text sf_admin_list_td_id">
  <?php echo link_to($chinchilla->getId(), 'chinchilla_edit', $chinchilla) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_codigo">
  <?php echo get_partial('chinchilla/codigo', array('type' => 'list', 'chinchilla' => $chinchilla)) ?>
</td>
<td class="sf_admin_date sf_admin_list_td_fechanac">
  <?php echo false !== strtotime($chinchilla->getFechanac()) ? format_date($chinchilla->getFechanac(), "dd-MM-y") : '&nbsp;' ?>
</td>
<td class="sf_admin_text sf_admin_list_td_color">
  <?php echo get_partial('chinchilla/color', array('type' => 'list', 'chinchilla' => $chinchilla)) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_sexo">
  <?php echo get_partial('chinchilla/sexo', array('type' => 'list', 'chinchilla' => $chinchilla)) ?>
</td>
<td class="sf_admin_boolean sf_admin_list_td_reproductor">
  <?php echo get_partial('chinchilla/list_field_boolean', array('value' => $chinchilla->getReproductor())) ?>
</td>
