<?php use_helper('I18N', 'Date') ?>
<?php include_partial('chinchilla/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('New Chinchilla', array(), 'messages') ?></h1>

  <?php include_partial('chinchilla/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('chinchilla/form_header', array('chinchilla' => $chinchilla, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('chinchilla/form', array('chinchilla' => $chinchilla, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('chinchilla/form_footer', array('chinchilla' => $chinchilla, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
