<td class="sf_admin_text sf_admin_list_td_id">
  <?php echo link_to($criadero->getId(), 'criadero_edit', $criadero) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_nombre">
  <?php echo $criadero->getNombre() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_usuario_id">
  <?php echo $criadero->getUsuarioId() ?>
</td>
<td class="sf_admin_date sf_admin_list_td_created_at">
  <?php echo false !== strtotime($criadero->getCreatedAt()) ? format_date($criadero->getCreatedAt(), "f") : '&nbsp;' ?>
</td>
<td class="sf_admin_date sf_admin_list_td_updated_at">
  <?php echo false !== strtotime($criadero->getUpdatedAt()) ? format_date($criadero->getUpdatedAt(), "f") : '&nbsp;' ?>
</td>
