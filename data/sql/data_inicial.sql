USE chinchis;
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-02-2014 a las 17:34:52
-- Versión del servidor: 5.1.66
-- Versión de PHP: 5.2.17

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `u100261343_chin`
--

--
-- Volcado de datos para la tabla `causa_fallecimiento`
--

INSERT INTO `causa_fallecimiento` (`id`, `descripcion`) VALUES
(5, 'Cuero'),
(6, 'Enfermedad'),
(7, 'Venta Animal'),
(8, 'Otros');

--
-- Volcado de datos para la tabla `color`
--

INSERT INTO `color` (`id`, `descripcion`) VALUES
(1, 'Beige Clara'),
(2, 'Beige Velvet'),
(3, 'Beige Rosa'),
(4, 'Gris EstÃ¡ndar'),
(5, 'Gris Oscuro'),
(6, 'Negro'),
(7, 'Negro Velvet');

--
-- Volcado de datos para la tabla `sexo`
--

INSERT INTO `sexo` (`id`, `corto`, `descripcion`) VALUES
(3, 'M', 'Macho'),
(4, 'H', 'Hembra');

--
-- Volcado de datos para la tabla `tipo_estado`
--

INSERT INTO `tipo_estado` (`id`, `descripcion`) VALUES
(1, 'Enfermo'),
(2, 'Tratamiento'),
(3, 'Saludable');

--
-- Volcado de datos para la tabla `tipo_sector`
--

INSERT INTO `tipo_sector` (`id`, `descripcion`) VALUES
(1, 'Sala de crÃ­a'),
(2, 'Sala de terminaciÃ³n');

--
-- Volcado de datos para la tabla `tipo_seguimiento`
--

INSERT INTO `tipo_seguimiento` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Individual', 'Historial personal de cada chinchilla'),
(2, 'Pareja', 'Historial de una pareja de chinchillas');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
